# Test Automation with Mocha & chai Framework 

This is a test project on web automation on Business Grants Portal 

# Web Automation with Cypress

**Objective:** 
  
  Application site is automated with Cypress and Mocha, IDE used is VisualStudio Code


## Table of Contents
> * [Introduction](#introduction)
> * [Preconditions](#preconditions)
> * [Installation](#installation)
> * [POM-FolderStructure](#pom_folderstructure)
> * [Versions](#versions)
> * [Syntax-Reference Docs](#syntax-reference)


## Introduction:

### About Cypress
   * Why Cypress
   Cypress can test anything that runs in a browser.
   Cypress enables you to write all types of tests using modern JavaScript frameworks like :

   * End-to-end tests
   * Integration tests
   * Unit tests

   
    
   * Reference Docs
   
      * [Cypress](https://blog.cloudboost.io/testing-your-frontends-javascript-code-with-cypress-io-c9d9a7102137)
      

## Preconditions:

  * _Set up IDE(VisualStudioCode-Recommended):_
  
    To download VisualStudioCode visit the website https://code.visualstudio.com/download 
    and Click the "DOWNLOAD" link under the Community Section.
    
  * _Node.Js:_

    Cypress need a working installation of Node.js on your system
    'https://nodejs.org/en/'
  
## Installation

### Install Cypress and setup for running the Test

Step 1: Open VisualStudio Code Terminal window

Step 2: Create a Folder and navigate to the folder

Step 3: Install Cypress

   `npm install cypress --save-dev`
step 4:Make sure the Cypress is set

   `npm cypress -v`


        
### Clone and Run Govtech Application Test case

Step 5: Clone  repository 

  `git clone <repository>`

Step 6: Save and Open the Cypress folder in your VSC 

Step 7: Open the Integration Folder - Resource Folder and verify the js test files(01.UserStory1-Eligibility.js,02.UserStory2- Contact_Details.js, 03.UserStory3- Form Submission.js) 

Step 8: Open cypress through VSC terminal

   `npx cypress open`

Step 8: Open the Resource folder under Integration in Cypress 
  
Step 9: Run the .js test files through Cypress UI or Command line
 `npx cypress run --spec <Filename.js>`


Step 10: View the Test results in Cypress UI left pane or the report folder

  
## POM-FolderStructure
 

 Support -> **Command.js** file is used for keeping all Custom Commands for user flows
 
 Report folder will have test execution reports in html and json format.
 **mochaawesome report generator** is used for report creatiton.
 
    

## Versions

  cypress version :6.14.5
  Node.js Version : 6:14.5
  mochawesome version:6.1.1

## Syntax-Reference Docs  
   * [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)
   * [Chai- Assertion](https://github.com/chaijs/chai)
   * [Chai- Query](https://github.com/chaijs/chai-jquery)
   * [Common - Assertion](https://docs.cypress.io/guides/references/assertions.html#Common-Assertions)
   * [CSS Selector Reference](https://www.w3schools.com/cssref/css_selectors.asp)



## Thank you...
     
   
    
    
    
    