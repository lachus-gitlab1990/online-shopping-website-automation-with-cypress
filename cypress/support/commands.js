// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import './commands'
import 'cypress-plugin-snapshots/commands';
import Locators from '../integration/PageObjects/Locators.json'

const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const testname = `testname${id}`
    const Passwd = 'testinguser'

Cypress.Commands.add('Signup' ,() => {
    
    cy.request({
    url:"https://juice-shop.herokuapp.com/api/Users/",
    method: 'POST',
    body:{
      email: testname,
      password: Passwd,
      passwordRepeat: "testinguser",
       securityAnswer: "123",
       securityQuestion: {
         id: "2", question: "Mother's maiden name?"
        }}

    }).then(res => cy.log(res.body))
  
 })


 Cypress.Commands.add('Login-Token' ,() => {
    
    cy.request({
    url:"https://juice-shop.herokuapp.com/rest/user/login",
    method: 'POST',
    body:{email: testname, password: Passwd}

    }).then(res => localStorage.setItem('jwt',res.body.authentication.token))
    cy.visit('/')
 })

 Cypress.Commands.add('Login' ,() => {
    
    cy.visit('/')
    cy.Dismisspopup()
    cy.get(Locators.Account_Btn).click()
    cy.get(Locators.Login_Btn).click()
    cy.get(Locators.Email_id).type(testname)
    cy.get(Locators.Pwd_id).type(Passwd)
    cy.get(Locators.Click_Login).click()

 })

 Cypress.Commands.add('Dismisspopup', () => {
    cy.get("body").then($body => {
        if ($body.find('[aria-label="Close Welcome Banner"]').length > 0) {
            cy.contains("Dismiss", {
                matchCase: false
            }).click();
        }
    })
})

Cypress.Commands.add('Search_Item' ,() => {
    cy.get(Locators.Search_id).type('Apple')
    cy.get(Locators.Add_to_Cart).first().click()
   
    
 })

 Cypress.Commands.add('Cart_Calulation' ,() => {
    cy.get(Locators.Cart).click()
    
 })

 Cypress.Commands.add('Checkout_process' ,() => { 
     cy.wait(2000)
     cy.reload()
    cy.get(Locators.Checkout).click()
    
 })

 Cypress.Commands.add('Add_New_Address' ,() => { 
    cy.get(Locators.Add_New_Address).click()
    cy.get('#address-form').click()
    cy.get(Locators.Country).type('Singapore')
    cy.get(Locators.Name).type('NewTestname')
    cy.get(Locators.Mobile).type('63197988')
    cy.get(Locators.ZIP).type('544200')
    cy.get(Locators.Address).type('Singapore address')
    cy.get(Locators.City).type('Singapore')
    cy.get(Locators.State).type('Singapore')
    cy.get(Locators.Submit).click()
    cy.get(Locators.Choose_Address).check()
    
 })

 Cypress.Commands.add('Choose_delivery_speed' ,() => { 
     cy.get(Locators.Choose_deliveryspeed).check()
 })
